#ifndef JSON_PARSER_H
#define JSON_PARSER_H

#include <string>
#include <map>
#include <vector>

class Json_Parser
{
public:
    Json_Parser(void) {}

    bool parse(const std::string&);

    bool has(const std::string&);
    std::string getRaw(const std::string&) const; ///content with " ", if there is
    std::string getContent(const std::string&) const; ///content excluding  " "
    Json_Parser getParser(const std::string&); ///

    bool getArraySafe(const std::string&, std::vector<std::string>&) const;
    std::vector<std::string> getArray(const std::string&) const; ///content in [ {..},{..} ]

    void printContent(void) const;

    static bool extractItems(std::string str, std::vector<std::string>& items);

    static std::string make(const std::string str1, const std::string str2);
    static std::string make(const std::vector< std::pair<std::string,std::string> >& args);

private:

    std::map<std::string, std::string> mContent;
};

#endif // JSON_PARSER_H
