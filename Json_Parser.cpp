#include "Json_Parser.hpp"


/*

{
    "a" : "x",
    "b" : "y",
    "d" : 18
}


*/

#include <iostream>

std::string Json_Parser::make(const std::string str1, const std::string str2)
{
    std::string sep;
    sep.push_back('"');
    return sep + str1 + sep + ": " + sep + str2 + sep;
}

std::string Json_Parser::make(const std::vector< std::pair<std::string,std::string> >& args)
{
    std::string message = "{";
    for(std::size_t i = 0; i < args.size(); i++)
    {
        message += Json_Parser::make( args[i].first, args[i].second );

        if( i+1 != args.size())
        {
            message += ",";
        }
    }

    message += "}";
    return message;
}

bool Json_Parser::extractItems(std::string str, std::vector<std::string>& items)
{
    while(true)
    {
        if(str.empty())
        {
            break;
        }

        if(str.front() == ' ')
        {
            str.erase( str.begin() );
        }
        else
        {
            break;
        }
    }

    while(true)
    {
        if(str.empty())
        {
            break;
        }

        if(str.back() == ' ')
        {
            str.erase( std::next(str.rbegin()).base() );
        }
        else
        {
            break;
        }
    }

    if(str.size() < 2)
    {
        std::cout << "size < 2" << std::endl;
        return false;
    }


    if(str[0] != '[' or str.back() != ']')
    {
        std::cout << "can't detect propper delimiter: " << str[0] << " " << str.back() << std::endl;
        return false;
    }

    std::size_t lastPos = 0;
    while(true)
    {
        std::size_t openBracket = str.find('{', lastPos);
        if(openBracket == std::string::npos)
        {
            break;
        }

        std::size_t closeBracket = str.find('}', openBracket);
        if(closeBracket == std::string::npos)
        {
            return false;
        }

        lastPos = closeBracket;

        //std::cout << str.size() << " " << openBracket << " " << closeBracket << std::endl;
        items.push_back( std::string(str.begin() + openBracket, str.begin() + closeBracket+1 ) );
    }

    return true;
}

std::size_t getEnd(const std::string& str)
{
    if(str.empty() or (str.front() != '{' and str.front() != '[') )
    {
        return std::string::npos;
    }

    char beginDelim = '{';
    char endDelim = '}';

    if(str.front() == '[')
    {
        beginDelim = '[';
        endDelim = ']';
    }

    int open = 0;
    int close = 0;

    for(std::size_t i = 0; i < str.size(); i++)
    {
        if(str[i] == beginDelim)
        {
            open++;
        }

        if(str[i] == endDelim)
        {
            close++;
        }

        if(open == close)
        {
            return i;
        }
    }

    return std::string::npos;
}

bool Json_Parser::parse(const std::string& toParse)
{
    if(toParse.size() < 3)
    {
        return false;
    }

    if(toParse.front() != '{' or toParse.back() != '}')
    {
        std::cout << "parse 1 " << toParse.front() << " " << toParse.back() << std::endl;
        return false;
    }

    std::size_t lastContent = 0;
    while(true)
    {
        const std::size_t nextQuote = toParse.find('"', lastContent);
        if(nextQuote == std::string::npos)
        {
            ///escape i guess
            break;
        }

        const std::size_t nextNextQuote = toParse.find('"', nextQuote+1);
        if(nextNextQuote == std::string::npos)
        {
            ///escape i guess
            break;
        }

        const std::string key( toParse.begin()+nextQuote+1, toParse.begin()+nextNextQuote );

        std::size_t nextColon = toParse.find(':', nextNextQuote);
        if(nextColon == std::string::npos)
        {
            break;
        }
        nextColon++;

        std::size_t nextComma = toParse.find(',', nextColon);
        if(nextComma == std::string::npos)
        {
            nextComma = toParse.size()-1;
            //break;
        }

        std::size_t beginContent = nextColon;
        while(true)
        {
            if(beginContent >= nextComma)
            {
                break;
            }

            if(toParse[beginContent] == ' ')
            {
                beginContent++;
            }
            else
            {
                break;
            }
        }

        if(toParse[beginContent] == '{' or toParse[beginContent] == '[')
        {
            nextComma = getEnd( std::string( toParse.begin()+beginContent, toParse.end() ) );

            if(nextComma == std::string::npos)
            {
                break;
            }

            nextComma += beginContent+1;
        }

        if(nextComma == std::string::npos)
        {
            break;
        }


        const std::string content( toParse.begin()+beginContent, toParse.begin()+nextComma );

        mContent[key] = content;

        //std::cout << "key: |" << key << "| content: |" << content << "|" << std::endl;

        lastContent = nextComma;
    }

    return true;
}

bool Json_Parser::has(const std::string& key)
{
    return mContent.count(key);
}

std::vector<std::string> Json_Parser::getArray(const std::string& key) const
{
    std::vector<std::string> vec;

    getArraySafe(key,vec);

    return vec;
}

bool Json_Parser::getArraySafe(const std::string& key, std::vector<std::string>& extracted) const
{
    extracted.clear();
    if(!mContent.count(key) or mContent.at(key).empty())
    {
        return false;
    }

    const std::string& content = mContent.at(key);

    if(content.front() != '[' or content.back() != ']')
    {
        return false;
    }

    std::size_t pos = 1;
    while(true) ///[ {..},{..} ]
    {
        const std::size_t nextOpen = content.find('{', pos);
        if(nextOpen == std::string::npos)
        {
            break;
        }

        //std::cout << std::string( content.begin()+nextOpen, content.end() ) << " " << getEnd( std::string( content.begin()+nextOpen, content.end() ) ) << std::endl;

        std::size_t nextClose = getEnd( std::string( content.begin()+nextOpen, content.end() ) );
        if(nextClose == std::string::npos)
        {
            std::cout << "getArraySafe can't find nextclose." << std::endl;
            return false;
        }

        nextClose += nextOpen+1;

        extracted.push_back( std::string( content.begin()+nextOpen, content.begin()+nextClose ) );
        pos = nextClose;
    }


    ///is there anything left that isn't some whitespaces ?
    for(std::size_t i = pos; i+1 < content.size(); i++)
    {
        if(content[i] != ' ')
        {
            std::cout << "getArraySafe doesnt end with white space. |" << content[i] << "| " << i << " " << content.size()  << std::endl;
            return false;
        }
    }

    return true;
}

std::string Json_Parser::getRaw(const std::string& key) const
{
    if(!mContent.count(key))
    {
        return "";
    }

    return mContent.at(key);
}

std::string Json_Parser::getContent(const std::string& key) const
{
    if(!mContent.count(key))
    {
        return "";
    }

    const std::string content = mContent.at(key);

    std::size_t first = content.find('"');
    if(first == std::string::npos)
    {
        return content;
    }

    std::size_t last = content.rfind('"');
    if(last == std::string::npos or last == first)
    {
        return content;
    }

    return std::string( content.begin()+first+1, content.begin()+last );
}


Json_Parser Json_Parser::getParser(const std::string& key)
{
    Json_Parser parser;

    if(!mContent.count(key))
    {
        return parser;
    }

    if( !parser.parse(getRaw(key)) )
    {
        ///do nothing lol
    }

    return parser;
}

void Json_Parser::printContent(void) const
{
    for(auto it = mContent.begin(); it != mContent.end(); it++)
    {
        std::cout << it->first << " : " << it->second << std::endl;
    }
}

